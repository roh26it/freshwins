var fs = require('fs')
  , jade = require('jade')
  , sys = require('sys')
  , exec = require('child_process').exec;

var Converter = require("csvtojson").Converter;
var converter = new Converter({});
converter.fromFile("./file.csv",function(err,result){
  // Render the jade file to html first
  var html = jade.compileFile('./views/index.jade', {})({items:result});
  fs.writeFile('./index.html',html,function(err,result){
    console.log("Step 1/2: File written");
    // Create the PDF from the HTML file
    exec("./phantomjs phantom.js "+fs.realpathSync('./index.html'),function(error, stdout, stderr) {
      sys.puts(error);
      sys.puts(stdout);
      sys.puts(stderr);
      console.log("Step 2/2: Check out your new freshwins.pdf in this folder!");
   });
  })
});