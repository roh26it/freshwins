var page = require('webpage').create()
  , system = require('system');

address = system.args[1];

page.open(address, function (status) {
  if (status !== 'success') {
      console.log('Unable to load the address!');
      phantom.exit(1);
  } else {
    window.setTimeout(function () {
      page.render("freshwins.pdf");
      phantom.exit();
    }, 200);
  }
});