var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/', function(req, res, next) {
  var Converter = require("csvtojson").Converter;
  var converter = new Converter({});
  converter.fromFile("./file.csv",function(err,result){
    console.log(err,result);
    res.render('index', { title: 'Express', items: result });
  });
});

module.exports = router;
