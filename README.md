# README #

### What is this repository for? ###

This is the Freshwins format generator. It takes in a CSV file and outputs formatted HTML in the Freshwins format. Meant to be an easy way to build the Freshwins file.

Version 0.1

### How do I get set up? ###

This does not need a database setup. Following are the instructions to setup and get the Generator up and running. (Mac OSX only)

#### 1. Install Node.JS ####
Install node.js from the Terminal. Type:

`brew install node`

Or install it from their website: https://nodejs.org/en/

To verify if node got installed properly, open the Terminal and type `node -v` ↵. It should print out the node version. (Should be 6.2.2 on Jul 4, 16)

#### 2. Setup Git ####
Install Sourcetree. You can download it here: https://www.sourcetreeapp.com/


OR

Type this command in the Terminal:

`brew install git` 

This should install git on the mac. 

Clone the repository with these commands in the terminal. Ask Rohit for the password if it asks for one

`cd ~` 

`git clone https://bitbucket.org/roh26it/freshwins.git` 

This would download a copy of the source code for the generator on your local machine.

#### 2. Setup the Generator ####
Go inside the folder in the terminal

`cd ~/freshwins`  

Install the libraries being used

`npm install` 

#### 3. Run the server ####
Fill the format file in [Google Sheets](https://docs.google.com/spreadsheets/d/1okpLGB8Km69WWisHEvyIfOg8PADPatzU9eVpgJKWa-8/edit#gid=0) and download the CSV version of it

![http://i.imgur.com/tnvx71p.png?1](http://i.imgur.com/tnvx71p.png?1)

Rename the file to file.csv and copy it to the freshwins folder.

Run this command in the terminal to start the server:

`npm start`

#### 4. Done! ####
In the browser, navigate to [localhost:5000](http://localhost:5000) and you should be able to check the freshwins format.